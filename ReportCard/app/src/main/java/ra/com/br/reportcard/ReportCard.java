package ra.com.br.reportcard;

public class ReportCard {

    private String subject;

    public ReportCard(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
